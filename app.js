//app.js
// 引入SDK核心类
var QQMapWX = require('/utils/qqmap-wx-jssdk.min.js')

App({
  globalData: {
    // ..其他全局变量..
    patrolForm: null,
      
    // 实例化API核心类
    qqmapsdk: new QQMapWX({
      key: '这个key是你申请下来的key' // 必填
    }),
      
    // ..其他全局变量..
  },
  // 其他代码
})